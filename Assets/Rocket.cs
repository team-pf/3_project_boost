﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour {

    Rigidbody rigidBody;
    bool    soundPlaying;
    AudioSource audiosource;
    float startVolume;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        audiosource = GetComponent<AudioSource>();
        audiosource.Play();
        audiosource.Pause();
        startVolume = audiosource.volume;
	}
	
	// Update is called once per frame
	void Update () {
        ProcessInput();	
	}

    //Use StartCoroutine();
    IEnumerator VolumeFade(AudioSource _AudioSource, float _EndVolume, float _FadeLength)
    {
        float _StartTime = Time.time;

        while (!soundPlaying &&
               Time.time < _StartTime + _FadeLength)
        {

            _AudioSource.volume = startVolume + ((_EndVolume - startVolume) * ((Time.time - _StartTime) / _FadeLength));

            yield return null;

        }

        if (_EndVolume == 0) { _AudioSource.UnPause(); }

    }

    private void ProcessInput()
    {
        if (Input.GetKey(KeyCode.Space)) // Thrusting
        {
            rigidBody.AddRelativeForce(Vector3.up);
            if (!soundPlaying)
            {
                soundPlaying = true;
                audiosource.volume = startVolume;
                audiosource.UnPause();
            }
        }
        else
        {
            if (soundPlaying)
            {
                soundPlaying = false;
                StartCoroutine(VolumeFade(audiosource, 0f, 0.5f));
            }
        }

        const float rcsThrust = 1.0f;
        float rotation = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A)) // Rotate left
        {
            transform.Rotate(Vector3.forward * rotation);
        }
        else if (Input.GetKey(KeyCode.D)) // Rotate right
        {
            transform.Rotate(-Vector3.forward * rotation);
        }
    }
}

